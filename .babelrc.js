module.exports = (api) => {
  // base config for rollup
  const babelPresetEnv = ['@babel/preset-env', { modules: false }];
  const config = {
    presets: [babelPresetEnv],
    plugins: [
      '@babel/plugin-proposal-nullish-coalescing-operator',
      '@babel/plugin-proposal-optional-chaining',
      '@babel/plugin-syntax-dynamic-import',
    ],
  };

  // storybook and visual regression tests
  if (api.env('storybook')) {
    babelPresetEnv[1] = { targets: { esmodules: true } };
  }

  /*  // jest tests
  if (api.env('test')) {
    // tests are run in a node environment, not a browser
    babelPresetEnv[1] = { targets: { node: 'current' } };
    config.plugins.push('require-context-hook');
  } */

  return config;
};
