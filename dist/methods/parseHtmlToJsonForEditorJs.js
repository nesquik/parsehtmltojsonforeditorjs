function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var ALLOWED_TAGS = ['rp', 'rt', 'ruby', 'mark', 'i', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'center', 'strong', 'em', 'u', '#text'];

var getElement = function getElement(content) {
  var parser = new DOMParser();
  return parser.parseFromString(content, 'text/html');
};

var getInfoElementForEditor = function getInfoElementForEditor(element) {
  var type;
  var infoElement;
  var items;
  var data = {};
  var result = [];

  switch (element.nodeName.toUpperCase()) {
    case 'P':
      if (element.childNodes) {
        Array.from(element.childNodes).forEach(function (el) {
          if (ALLOWED_TAGS.includes(el.nodeName.toLowerCase())) {
            return;
          }

          element.removeChild(el);
          infoElement = getInfoElementForEditor(el);

          if (!infoElement.type) {
            result.push({
              type: 'paragraph',
              data: {
                text: el.innerText
              }
            });
          } else {
            result.push({
              data: infoElement.data,
              type: infoElement.type
            });
          }
        });

        if (!result.length) {
          return {
            type: 'paragraph',
            data: {
              text: element.innerHTML.replace('strong', 'b').replace('/strong', '/b').replace('/em', '/i').replace('em', 'i')
            }
          };
        }

        if (result.length && element.innerHTML.length) {
          result.push({
            type: 'paragraph',
            data: {
              text: element.innerHTML.replace('strong', 'b').replace('/strong', '/b').replace('/em', '/i').replace('em', 'i')
            }
          });
        }

        return _objectSpread2({}, result);
      }

      type = 'paragraph';
      data.text = element.innerText;
      break;

    case 'IFRAME':
      type = 'embed';
      data.embed = element.src;
      data.source = element.src;
      data.caption = '';
      data.height = 320;
      data.width = 580;
      data.service = 'youtube';
      break;

    case 'OL':
      items = Array.from(element.children).map(function (el) {
        if (el.childNodes) {
          Array.from(el.childNodes).forEach(function (child) {
            if (ALLOWED_TAGS.includes(child.nodeName.toLowerCase())) {
              return;
            }

            el.removeChild(child);
            infoElement = getInfoElementForEditor(child);
            result.push({
              data: infoElement.data,
              type: infoElement.type
            });
          });
        }

        return el.innerHTML.replace('strong', 'b').replace('/strong', '/b').replace('/em', '/i').replace('em', 'i');
      });
      result.push({
        data: {
          items: items,
          style: 'ordered'
        },
        type: 'list'
      });
      return _objectSpread2({}, result);

    case 'UL':
      items = Array.from(element.children).map(function (el) {
        if (el.childNodes) {
          Array.from(el.childNodes).forEach(function (child) {
            if (ALLOWED_TAGS.includes(child.nodeName.toLowerCase())) {
              return;
            }

            el.removeChild(child);
            infoElement = getInfoElementForEditor(child);
            result.push({
              data: infoElement.data,
              type: infoElement.type
            });
          });
        }

        return el.innerHTML.replace('strong', 'b').replace('/strong', '/b').replace('/em', '/i').replace('em', 'i');
      });
      result.push({
        data: {
          items: items
        },
        type: 'list'
      });
      return _objectSpread2({}, result);

    case 'IMG':
      type = 'image';
      data.file = {
        url: element.src
      };
      break;

    case 'BR':
      type = 'paragraph';
      data.text = '</br>';
      break;

    case 'H1':
      type = 'header';
      data.text = element.innerText;
      data.level = 1;
      break;

    case 'H2':
      type = 'header';
      data.text = element.innerText;
      data.level = 2;
      break;

    case 'H3':
      type = 'header';
      data.text = element.innerText;
      data.level = 3;
      break;

    case 'H4':
      type = 'header';
      data.text = element.innerText;
      data.level = 4;
      break;

    case 'H5':
      type = 'header';
      data.text = element.innerText;
      data.level = 5;
      break;

    case 'H6':
      type = 'header';
      data.text = element.innerText;
      data.level = 6;
      break;

    case 'SCENARIO':
      type = 'scenarioLink';
      data.title = element.innerText;
      data.id = element.getAttribute('hinted-link');
      break;
  }

  return {
    type: type,
    data: data
  };
};

var parseHtmlToJsonForEditorJs = function parseHtmlToJsonForEditorJs(html) {
  var contentJSON = {
    blocks: []
  };
  var el = getElement(html);
  Array.from(el.body.children).forEach(function (elem) {
    var parsJson = getInfoElementForEditor(elem);

    if (parsJson !== null && parsJson !== void 0 && parsJson.type) {
      contentJSON.blocks.push(parsJson);
      return;
    }

    Object.values(parsJson).forEach(function (obj) {
      contentJSON.blocks.push(obj);
    });
  });
  return contentJSON;
};

export { parseHtmlToJsonForEditorJs };
