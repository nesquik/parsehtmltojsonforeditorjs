const ALLOWED_TAGS = [
  'rp',
  'rt',
  'ruby',
  'mark',
  'i',
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'h6',
  'center',
  'strong',
  'em',
  'u',
  '#text',
];

const getElement = (content) => {
  const parser = new DOMParser();
  return parser.parseFromString(content, 'text/html');
};

const getInfoElementForEditor = (element) => {
  let type;
  let infoElement;
  let items;
  
  const data = {};
  const result = [];

  switch (element.nodeName.toUpperCase()) {
    case 'P':
      if (element.childNodes) {
        Array.from(element.childNodes).forEach((el) => {
          if (ALLOWED_TAGS.includes(el.nodeName.toLowerCase())) {
            return;
          }
          element.removeChild(el);
          infoElement = getInfoElementForEditor(el);

          if (!infoElement.type) {
            result.push({  type: 'paragraph', data: { text: el.innerText } });
          } else {
            result.push({ data: infoElement.data, type: infoElement.type });
          }
        });

        if (!result.length) {
          return {
            type: 'paragraph',
            data: {
              text: element.innerHTML
                  .replace('strong', 'b')
                  .replace('/strong', '/b')
                  .replace('/em', '/i')
                  .replace('em', 'i'),
            },
          };
        }
        
        if (result.length && element.innerHTML.length) {
          result.push({
            type: 'paragraph',
            data: {
              text: element.innerHTML
                  .replace('strong', 'b')
                  .replace('/strong', '/b')
                  .replace('/em', '/i')
                  .replace('em', 'i'),
            },
          });
        }

        return { ...result };
      }
      
      type = 'paragraph';
      data.text = element.innerText;
      break;
    case 'IFRAME':
      type = 'embed';
      data.embed = element.src;
      data.source = element.src;
      data.caption = '';
      data.height = 320;
      data.width = 580;
      data.service = 'youtube';
      break;
    case 'OL':
      items = Array.from(element.children).map((el) => {
        if (el.childNodes) {
          Array.from(el.childNodes).forEach((child) => {
            if (ALLOWED_TAGS.includes(child.nodeName.toLowerCase())) {
              return;
            }
            
            el.removeChild(child);
            infoElement = getInfoElementForEditor(child);
            
            result.push({ data: infoElement.data, type: infoElement.type });
          });
        }
        
        return el.innerHTML
            .replace('strong', 'b')
            .replace('/strong', '/b')
            .replace('/em', '/i')
            .replace('em', 'i');
      });
      
      result.push({
        data: {
          items,
          style: 'ordered',
        },
        type: 'list',
      });
      
      return { ...result };
    case 'UL':
      items = Array.from(element.children).map((el) => {

        if (el.childNodes) {
          Array.from(el.childNodes).forEach((child) => {
            if (ALLOWED_TAGS.includes(child.nodeName.toLowerCase())) {
              return;
            }
            
            el.removeChild(child);
            infoElement = getInfoElementForEditor(child);
            
            result.push({ data: infoElement.data, type: infoElement.type });
          });
        }
        
        return el.innerHTML
            .replace('strong', 'b')
            .replace('/strong', '/b')
            .replace('/em', '/i')
            .replace('em', 'i');
      });
      
      result.push({
        data: {
          items,
        },
        type: 'list',
      });
      
      return { ...result };
    case 'IMG':
      type = 'image';
      data.file = { url: element.src };
      break;
    case 'BR':
      type = 'paragraph';
      data.text = '</br>';
      break;
    case 'H1':
      type = 'header';
      data.text = element.innerText;
      data.level = 1;
      break;
    case 'H2':
      type = 'header';
      data.text = element.innerText;
      data.level = 2;
      break;
    case 'H3':
      type = 'header';
      data.text = element.innerText;
      data.level = 3;
      break;
    case 'H4':
      type = 'header';
      data.text = element.innerText;
      data.level = 4;
      break;
    case 'H5':
      type = 'header';
      data.text = element.innerText;
      data.level = 5;
      break;
    case 'H6':
      type = 'header';
      data.text = element.innerText;
      data.level = 6;
      break;
    case 'SCENARIO':
      type = 'scenarioLink';
      data.title = element.innerText;
      data.id = element.getAttribute('hinted-link');
      break;
    default:
      break;
  }

  return {
    type,
    data,
  };
};

export const parseHtmlToJsonForEditorJs = (html) => {
  const contentJSON = {
    blocks: [],
  };
  const el = getElement(html);

  Array.from(el.body.children).forEach((elem) => {
    const parsJson = getInfoElementForEditor(elem);

    if (parsJson?.type) {
      contentJSON.blocks.push(parsJson);
      return;
    }

    Object.values(parsJson).forEach((obj) => {
      contentJSON.blocks.push(obj);
    });
  });

  return contentJSON;
};