# Parse HTML to JSON for EditorJS

## Project setup

```
yarn
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```
